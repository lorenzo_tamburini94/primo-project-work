<?php
/**
 * Template part for displaying page content in page-fimografia.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package x-blog
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( get_the_title());

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'x-blog' ),
				'after'  => '</div>',
			) );
		?>

		<?php
			//assegno i dati dei custom fields a delle variabili
			$trama = get_post_meta(get_the_ID(),'trama',true);
			$cast = get_post_meta(get_the_ID(),'cast',true);
			$durata = get_post_meta(get_the_ID(),'durata',true);
			$annopubblicazione = get_post_meta(get_the_ID(),'anno_di_pubblicazione',true);

			echo "trama $trama";

		?>
	</div><!-- .entry-content -->

	

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						wp_kses(
							/* translators: %s: Name of current post. Only visible to screen readers */
							__( 'Edit <span class="screen-reader-text">%s</span>', 'x-blog' ),
							array(
								'span' => array(
									'class' => array(),
								),
							)
						),
						get_the_title()
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
