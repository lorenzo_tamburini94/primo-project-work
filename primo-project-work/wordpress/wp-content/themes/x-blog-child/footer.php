<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package x-blog
 */
$xblog_theme = wp_get_theme();
$xtheme_domain = $xblog_theme->get( 'TextDomain' );
if( $xtheme_domain == 'x-magazine' ){
	$theme_slug = $xtheme_domain; 
}else{
	$theme_slug = 'x-blog'; 
}

?>

	</div><!-- #content -->
    <?php if(is_dynamic_sidebar('footer-widget')): ?>
    <div class="footer-widget-area"> 
        <div class="baby-container widget-footer"> 
            <?php dynamic_sidebar('footer-widget'); ?>
        </div>
    </div>
    <?php endif; ?>
	<footer id="colophon" class="site-footer footer-display">
		<div class="baby-container site-info">
				<?php
				//modifico il footer del sito
				echo "Primo project work IFTS 2019-2020 <br> Realizzato da Lorenzo Tamburini";
				?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
