<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fellini_100' );

/** MySQL database username */
define( 'DB_USER', 'userwp' );

/** MySQL database password */
define( 'DB_PASSWORD', 'userwp' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
	define('AUTH_KEY',         '|q${No>jvNghM--1`#p[Y(L>~h!1$E)vKlsuFJWIo+xM?^Jc^j,8G9|lnsKh&2wR');
	define('SECURE_AUTH_KEY',  '#TuBNiqpqslv3u}dmaDn)lL}y|&2</poQI7j9::CV RD8>fiJE|>k4yR.C]AX$dP');
	define('LOGGED_IN_KEY',    'Hn+P}<:Cprw0q1`k=zp;KYH;.f67-1|@M(4T(D/rgQ|/H+2%ohB+^K*mu)h,n?|-');
	define('NONCE_KEY',        '2$EohRJ>dC#1G%or4u}Hil]o?4]lr,y-aOC{#&.cWqec+K&GUnH)^|TRgtd:W%:J');
	define('AUTH_SALT',        'Pq|qrUEj|5hJD7gVZu1I}s@FgXQ,,Sg.7@B3CZKia&Px=PFdgST7g^~G3gp4K>+p');
	define('SECURE_AUTH_SALT', '[wV^`8B69J@_(;-}]E!3fs#,#G.mlJqI5y=!*2}X5Y8t(np1`6/0,QO:7Jfb<W~7');
	define('LOGGED_IN_SALT',   'DdRxEv<|vM /{:$Y,+[CXn)afe}^cS()r(6/iR7tkg!>4@04H?HNZoXm(2|jYi}<');
	define('NONCE_SALT',       ')M*b.It,QR[Lkj:pSoV!J;U zUi3#?Q^V$q=+:Oy?{<y7hRE6to$W/{q-%UD}u^}');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
